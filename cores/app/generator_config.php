<?php 

// $data = '';

$config_data = '<?php 
// Database Connection
$server = "'.$server.'";		//Server name
$username = "'.$username.'";			//database Username
$db_password = "'.$db_password.'";			//database Password


try {
	$ATTR = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];
	// quotation always double ""
	$con = new PDO("mysql:host=$server;dbname='.$db_name.'", $username, $db_password, $ATTR);
	// echo "connected";  //Display text if connected
} catch (PDOException $e) {
	 echo "Connection failed: " . $e->getMessage();
}

 ?>';


 $core_data = '<?php 


	function insert($con,$table,$arrs){
		$parameter = array();
		$data = array();
		$dataparam = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key;
			$dataparam[] = ":".$key;
			$data[$key] = $value;
		}

		$sql = "INSERT INTO ".$table."(".implode(",", $parameter).") VALUES(".implode(",", $dataparam).")";

		if (save($con,$data,$sql) > 0) {
			echo 1;
		}else{
			echo save($con,$data,$sql);
		}
		
	}


	function update($con,$table,$arrs){
		$get_parameter = array();
		$parameter = array();
		$data = array();
		$dataparam = array();
		$selector = "";
		$data_update = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key.":".$key;
			$data[$key] = $value;
		}


		for ($i=0; $i < count($parameter); $i++) { 
			if ($i == 0) {
				$selector = $parameter[$i];
			}else{
				$get_parameter[] = $parameter[$i];
			}
		}


		$sql = "UPDATE ".$table." set ".implode(",", $get_parameter)." WHERE ".$selector;

		if (save($con,$data,$sql) > 0) {
			echo 1;
		}else{
			echo save($con,$data,$sql);
		}
		
	}

	// only 1 parameter
	function delete($con,$table,$arrs){
		$get_parameter = array();
		$parameter = array();
		$data = array();
		$dataparam = array();
		$selector = "";
		$data_update = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key.":".$key;
			$data[$key] = $value;
		}


		for ($i=0; $i < count($parameter); $i++) { 
			if ($i == 0) {
				$selector = $parameter[$i];
			}else{
				$get_parameter[] = $parameter[$i];
			}
		}


		$sql = "DELETE FROM ".$table." WHERE ". $selector;

		if (save($con,$data,$sql) > 0) {
			echo 1;
		}else{
			echo save($con,$data,$sql);
		}
	}


 	function save($con,$data,$sql){
		// sample
		// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
		$prep = $con->prepare($sql);
		$prep->execute($data);

		if ($prep) {
			return 1;
		}
	}



	function fetch_record($con,$data,$sql){
		// sample
		// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
		$prep = $con->prepare($sql);
		$prep->execute($data);

		return $prep;
	}


	function verify_record($con,$data,$sql){
		$prep = $con->prepare($sql);
		$prep->execute($data);

		$row = $prep->fetch();

		return $prep->rowCount();
	}


 ?>';


 $function_data = '<?php 
 include("config/config.php");
 include("core/core.php");
 ?>';


 $index_data = '<?php include("function/function.php"); ?>
<!DOCTYPE html>
 <html lang="en">
 <head>
 	<title>CODYGENERATOR</title>
	<link rel="stylesheet" type="text/css" href="webroot/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="webroot/css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="webroot/css/animate.css">
	<link rel="stylesheet" type="text/css" href="webroot/font/css/all.css">
 </head>
 <body>
	 <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		  <ul class="navbar-nav">
		    <li class="nav-item active">
		      <a class="nav-link  text-info" href="#"><i class="fa fa-code"></i> <span style="color: orange;">CODY</span>GENERATOR</a>
		    </li>
		  <!--   <li class="nav-item">
		      <a class="nav-link" href="#">Link</a>
		    </li>
		    <li class="nav-item">
		      <a class="nav-link disabled" href="#">Disabled</a>
		    </li> -->
		  </ul>
		</nav>
 
 </body>
 <script type="text/javascript" src="webroot/js/jquery.min.js"></script>
 <script type="text/javascript" src="webroot/js/popper.min.js"></script>
 <script type="text/javascript" src="webroot/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="webroot/js/sweetalert.min.js"></script>

 </html>';
 
 ?>
 