<!DOCTYPE html>
<html lang="en">
<head>
	<title>CODYGENERATOR</title>
	<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="asset/css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="asset/css/animate.css">
	<link rel="stylesheet" type="text/css" href="asset/font/css/all.css">
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  <ul class="navbar-nav">
	    <li class="nav-item active">
	      <a class="nav-link  text-info" href="#"><i class="fa fa-code"></i> <span style="color: orange;">CODY</span>GENERATOR</a>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link pt-2" href="form_generator.php">Form Generator (HTML)</a>
	    </li>
	     <li class="nav-item">
	      <a class="nav-link pt-2" href="laravel_form.php">Form Generator (Laravel)</a>
	    </li>
	     <li class="nav-item">
	      <a class="nav-link pt-2" href="form_creator_cake.php">Form Generator (CakePHP)</a>
	    </li>
	    
	  <!--   <li class="nav-item">
	      <a class="nav-link" href="#">Link</a>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link disabled" href="#">Disabled</a>
	    </li> -->
	  </ul>
	</nav>
	<div class="container-fluid" id="hack_gif" style="display: none; color: transparent; width: 100% !important; height:100% !important; background-image: url(asset/img/locading.gif);">
		Please Wait...
	</div>
	
	<div class="container-fluid">
		<div class="row mt-5">
			<div class="col-sm-1"></div>
			<div class="col-sm-5">
				<div class="row">
					<div class="col-sm-12">
						<h1>Create Project</h1>
					</div>
					<div class="col-sm-12 form-group">
						<input type="text" id="project_name" class="form-control" placeholder="Enter your project name...">
					</div>
					<div class="col-sm-6 form-group">
						<input type="text" id="server" class="form-control" placeholder="Database Host" value="localhost">
					</div>
					<div class="col-sm-6 form-group">
						<input type="text" id="username" class="form-control" placeholder="Database Username" value="root">
					</div>
					<div class="col-sm-6 form-group">
						<input type="password" id="dbpassword" class="form-control" placeholder="Database password">
					</div>
					<div class="col-sm-6 form-group">
						<input type="text" id="dbname" class="form-control" placeholder="Database name" value="db_testing">
					</div>
					<div class="col-sm-12 text-right">
						<button class="btn btn-dark" onclick="create_project();">Test Connection</button>
					</div>
				</div>
			</div>
			
		<div class="col-sm-5">
				<div class="row">
					<div class="col-sm-12">
						<h1>My Project</h1>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Project Name</th>
									<th class="text-center" >Option</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$file_directory = 'project';

								$file =	scandir($file_directory);

								foreach ($file as $key => $value) {
									if ($key != 0 && $key != 1) {
										?>
										<tr>
											<td><?php echo $value ?></td>
											<td class="text-center">
												<a href="<?php echo $file_directory.'/'.$value ?>" class="btn btn-info">Open</a>
												<a href="cores/delete.php?url=<?php echo '../'.$file_directory.'/'.$value ?>" class="btn btn-danger">Delete</a>
											</td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
			</div>
		</div>

		<div class="col-sm-1"></div>
	</div>
</body>
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/popper.min.js"></script>
<script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
<script type="text/javascript" src="asset/js/sweetalert.min.js"></script>


<script type="text/javascript">
	function create_project(){
		var project_name = $("#project_name");
		var server = $("#server");
		var username = $("#username");
		var dbpassword = $("#dbpassword");
		var dbname = $("#dbname");


		if (project_name.val() == "") {
			project_name.focus();
			swal("Oops!","Please Enter your project name...","error");
		}
		else if (server.val() == "") {
			server.focus();
			swal("Oops!","Please Enter your server...","error");
		}
		else if (username.val() == "") {
			username.focus();
			swal("Oops!","Please Enter your db username...","error");
		}
		else if (dbname.val() == "") {
			dbname.focus();
			swal("Oops!","Please Enter your db name...","error");
		}else{
			var mydata = 'project_name=' + project_name.val() +'&server=' + server.val() +'&username=' + username.val() +'&dbpassword=' + dbpassword.val() +'&dbname=' + dbname.val();
			$.ajax({
				type:"POST",
				url:'cores/connect_test.php',
				data:mydata,
				dataType:'json',
				beforeSend:function(){
					swal("Loading!","Please Wait....","info");
					$("#hack_gif").show('fast');
				},
				success:function(data){
					// alert(data);
					if (data.result) {
						// window.location='Project_creator_JL/'+data.url;
						location.reload();
						
					}else{
						swal("Oops!",data.msg,"error");
					}
				}
			});
		}
	}
</script>

</html>