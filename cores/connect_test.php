<?php
// Database Connection
$project_name = $_POST['project_name'];
$server = $_POST['server'];		//Server name
$username = $_POST['username'];			//database Username
$db_password = $_POST['dbpassword'];			//database Password
$db_name = $_POST['dbname'];


$file_path = "../project/".$project_name;

include('app/generator_config.php');


if (file_exists($file_path)) {
	echo $project_name.' is already exist!';
}else{
	try {
		$ATTR = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];
		// quotation always double ""
		$con = new PDO("mysql:host=$server;dbname=$db_name", $username, $db_password, $ATTR);
		// echo 'Connection Success!';  //Display text if connected
		mkdir($file_path);
		include('app/file_location.php');
		include('app/core_creator.php');

		$array = array('result' => true, 'msg' => 'Connection Success!', 'url' => $file_path);
		echo json_encode($array);

	} catch (PDOException $e) {
		$array = array('result' => true, 'msg' =>  "Connection failed: " . $e->getMessage());
		echo json_encode($array);
	}
}
 ?>