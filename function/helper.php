<?php 

function tab($par){
	$result = '';
	for ($i=0; $i < $par ; $i++) { 
		$result .= "\t";
	}

	return $result;
}

function label($name){
	$str = explode('_', $name);
	$label = '';
	for ($i=0; $i <count($str) ; $i++) { 
		$label .= ucfirst($str[$i]).' ';
	}

	return $label;
}


function line($par){
	$result = '';
	for ($i=0; $i < $par ; $i++) { 
		$result .= "\n";
	}

	return $result;
}