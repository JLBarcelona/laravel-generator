<?php 

	// config
	$config_file = fopen($file_path."/config/config.php", "w") or die("Unable to open file!");
	fwrite($config_file, $config_data);
	fclose($config_file);



	// core
	$core_file = fopen($file_path."/core/core.php", "w") or die("Unable to open file!");
	fwrite($core_file, $core_data);
	fclose($core_file);


	// function
	$function_file = fopen($file_path."/function/function.php", "w") or die("Unable to open file!");
	fwrite($function_file, $function_data);
	fclose($function_file);


	// index
	$index_file = fopen($file_path."/index.php", "w") or die("Unable to open file!");
	fwrite($index_file, $index_data);
	fclose($index_file);

	// webroot
	$src = '../asset/';
	$dst = $file_path.'/webroot/';
	recurse_copy($src,$dst);


	function recurse_copy($src,$dst) {
	$dir = opendir($src);
		@mkdir($dst);
		while(false !== ( $file = readdir($dir)) ) {
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($src . '/' . $file) ) {
					recurse_copy($src . '/' . $file,$dst . '/' . $file);
				}
				else {
					copy($src . '/' . $file,$dst . '/' . $file);
				}
			}
		}
	closedir($dir);
	}
	
	
 ?>