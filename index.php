<!DOCTYPE html>
<html lang="en">
<head>
	<title>CODYGENERATOR</title>
	<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="asset/css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="asset/css/animate.css">
	<link rel="stylesheet" type="text/css" href="asset/font/css/all.css">
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  <ul class="navbar-nav">
	    <li class="nav-item active">
	      <a class="nav-link  text-info" href="#"><i class="fa fa-code"></i> <span style="color: orange;">CODY</span>GENERATOR</a>
	    </li>
	  </ul>
	</nav>
	<div class="container-fluid">
		<div class="row mt-4 m-1">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-4">
						<div class="card">
							<div class="card-header bg-primary text-white"><b>Laravel Crud Generator</b><span class="float-right"></span></div>
							<div class="card-body">
								<div class="row">
									<div class="col-sm-12 form-group">
										<label>Controller</label>
										<input type="text" name="controller" id="controller" class="form-control" value="SampleController" placeholder="Controller">
									</div>

									<div class="col-sm-6 form-group">
										<label>Model</label>
										<input type="text" name="model" id="model" class="form-control" value="Sample" placeholder="Model">
									</div>

									<div class="col-sm-6 form-group">
										<label>Primary ID</label>
										<input type="text" value="sample_id" name="base_id" id="base_id" class="form-control" placeholder="Primary ID">
									</div>

									<div class="col-sm-6 form-group ">
										<label>Prefix</label>
										<input type="text" name="basename" id="basename" value="samples" class="form-control" placeholder="prefix">
									</div>

									<div class="col-sm-6 form-group ">
										<label>Form ID</label>
										<input type="text" value="sample_form_id" name="form_id" id="form_id" class="form-control" placeholder="Form ID">
									</div>
								</div>
								<br>
							</div>
							<div class="card-footer"></div>
						</div>
					</div>

					

					<div class="col-sm-4">
						<div class="card">
							<div class="card-header bg-success text-white">FOR API</div>
							<div class="card-body small">
								<b class="h5">Instructions </b><small></small>
								<p class="mt-0 mb-2">1. Create your controller. </p>
								<p class="mt-1 mb-2">2. Fill up the form requirements.</p>
								<p class="mt-0 mb-2">3. Click the <b>Generate</b> button to generate the code. </p>

								<p class="mt-0 mb-2">4. Copy/Paste the <b>Route</b> codes to your laravel routes. </p>
								<p class="mt-0 mb-2">5. Copy/Paste the <b>Controller</b> codes to your laravel routes/api.php. </p>
								<p class="mt-0 mb-2">you can access the API like from the example. <br>ex: <strong>http://127.0.0.1/<b>api</b>/sample/list</b></strong>
								<br>
								<p class="mt-0 mb-2">NOTE: Make sure that your input name is the same with the <b>Database</b> field name.</p>


								<br>
							</div>
							<div class="card-footer"></div>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="card">
							<div class="card-header bg-success text-white">INSTALLATION FOR LARAVEL PROJECT</div>
							<div class="card-body small">
								<small>Supports: Bootstrap 4 and Laravel 7</small>
								<p class="bold">1.Include the <a href="asset/js/jl-validator.js" target="_blank">JL-VALIDATOR.js</a>.
								</p>
								<input type="text" class="text-center bg-dark border-0 rounded text-info small" readonly="" disabled="" style="width:250px;" value='<script src="../your-path/jl-validator.js"></script>'>
								<br>
								<a href="asset/js/jl-validator.js" target="_blank" class="btn btn-success mt-1 btn-sm">Download</a>
								<hr>
								<b class="h5">Instructions </b><small></small>
								<p class="mt-1 mb-2">1. Fill up the form requirements.</p>
								<p class="mt-0 mb-2">2. Add your form input. </p>
								<p class="mt-0 mb-2">NOTE: Make sure that your input name is the same with the <b>Database</b> field name.</p>
								<p class="mt-0 mb-2">3. Click the <b>Generate</b> button to generate the code. </p>

							</div>
							<div class="card-footer"></div>
						</div>
					</div>
				
					<div class="col-sm-12">
						<hr>
						<form id="all_form">
							<table class="table table-bordered table-sm">
								<thead>
									<tr>
										<th>INPUT TYPE</th>
										<th>ID/Name</th>
										<th>Class</th>
										<th>Placeholder / (Select)Options</th>
										<th width="20%">Column (mobile-sm-md-lg-xl)</th>
									</tr>
								</thead>
								
								<tbody id="form_inputs">

									<tr id="form_0">
										<td>
											<select class="form-control" id="input_type_0" oninput="type_input(this.value,'0');">
												<option value="text">Text</option>
												<option value="hidden">Hidden</option>
												<option value="number">Number</option>
												<option value="date">Date</option>
												<option value="select">Select</option>
												<option value="password">Password</option>
												<option value="email">Email</option>
												<option value="file">File</option>
											</select>
										</td>
										<td><input type="text" id="input_id_0" class="form-control" autocomplete="off"></td>
										<td><input type="text" id="input_class_0" class="form-control" autocomplete="off"></td>
										<td><input type="text" id="input_placeholder_0" class="form-control" autocomplete="off"></td>
										<td><input type="text" max="12" id="input_col_0" class="form-control" autocomplete="off" placeholder="Ex: col-sm-12 col-md-4"></td>
									</tr>
								</tbody>
							</table>
						</form>
						<input type="hidden" id="row_input_count" value="0">
					</div>

					<div class="col-sm-9 text-left">
						<button class="btn btn-danger  btn-sm " onclick="remove_form();"> -</button>
						<button class="btn btn-success mr-2 btn-sm " onclick="add_form();"> +</button>
					<br>
					<br>
					</div>
					<div class="col-sm-3">
						<button class="btn btn-info btn-sm btn-block" onclick="get_result_and_code();">Result</button>
						<br><br>
					</div>
					
				</div>
			</div>
			
		<!-- 	<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-12 form-group">
						<br><br>
						<div class="h3">Code Generator </div><small>By: JL Barcelona</small>
						<hr>
					<textarea class="form-control bg-dark text-light" readonly="" style="height:300px;" id="code_generator"></textarea> -->
				<!-- 	</div>
				</div>

			</div> -->
	</div>

	 <div class="modal fade" role="dialog" id="modal_generator">
	      <div class="modal-dialog modal-lg">
	        <div class="modal-content">
	          <div class="modal-header">
	            <div class="modal-title">
	           <div class="h5">Code Generator </div>
	            </div>
	            <button class="close" data-dismiss="modal">&times;</button>
	          </div>
	          <div class="modal-body">
	          	<small>Developed By: JL Barcelona</small>
	            <div id="code_generator" style="font-size: 3px;"></div>
	          </div>
	          <div class="modal-footer">
	            
	          </div>
	        </div>
	      </div>
	    </div>

	
</body>
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/popper.min.js"></script>
<script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
<script type="text/javascript" src="asset/js/sweetalert.min.js"></script>

<script type="text/javascript">
	var input_count = 0;
	var url = 'function/function.php';

	function type_input(type,id){

		if (type == 'select') {
			$("#input_placeholder_"+id).attr('placeholder', 'ex: Male-Female-others');
		}else if (type == 'date') {
			$("#input_placeholder_"+id).attr('disabled', true);
		}else{
			$("#input_placeholder_"+id).attr('disabled', false);
		}

	}

	function add_form(){
		var data = '';


		var input_count = (Number($('#row_input_count').val()) + 1);

		data += '<tr id="form_'+input_count+'">';
		data += '<td>';
		data += '<select class="form-control" id="input_type_'+input_count+'" oninput="type_input(this.value,'+input_count+');">';
		data += '<option value="text">Text</option>';
		data += '<option value="hidden">hidden</option>';
		data += '<option value="number">Number</option>';
		data += '<option value="date">Date</option>';
		data += '<option value="select">Select</option>';
		data += '<option value="password">Password</option>';
		data += '<option value="email">EMail</option>';
		data += '<option value="file">File</option>';
		data += '</select>';
		data += '</td>';
		data += '<td><input type="text" id="input_id_'+input_count+'" class="form-control" autocomplete="off"></td>';
		data += '<td><input type="text" id="input_class_'+input_count+'" class="form-control" autocomplete="off"></td>';
		data += '<td><input type="text" id="input_placeholder_'+input_count+'" class="form-control" autocomplete="off"></td>';
		data += '<td><input type="text" max="12" id="input_col_'+input_count+'" class="form-control" placeholder="Ex: col-sm-12 col-md-4" autocomplete="off"></td>';
		data += '</tr>';

		$("#form_inputs").append(data);
		$('#row_input_count').val(input_count);
	}

	function remove_form(){
		var input_count = $("#row_input_count").val();

	    if (input_count == 0) {
	      $("#livestock_count").val(0);
	    }else{
	      // $("#row_id_"+num).html('');
	      $("#form_"+input_count).remove("#form_"+input_count);
	      $("#row_input_count").val(input_count - 1);
	    }
	}

	function reset_form(){
	  document.getElementById('all_form').reset();
	}


	function get_result_and_code(){

		var controller = $("#controller");
		var model = $("#model");
		var basename = $("#basename");
		var form_id = $("#form_id");
		var base_id = $("#base_id");

 


		var number = Number($("#row_input_count").val());
		var input_data = [];
		for (var i = 0; i <= number; i++) {
		      input_data.push('['+$("#input_type_"+i).val() +'~'+ $("#input_id_"+i).val() +'~'+ $("#input_class_"+i).val()  +'~'+ $("#input_placeholder_"+i).val()+'~'+$("#input_col_"+i).val()+']');
		 }

		var mydata = 'action=laravel_form' + '&input_details=' + input_data + '&controller=' + controller.val() + '&form_id=' + form_id.val() + '&model=' + model.val() + '&basename=' + basename.val() + '&base_id=' + base_id.val();
		// alert(mydata);
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			dataType:'json',
			success:function(data){
				// console.log(data);
				// $("#html_results").html(data.data);
				$("#code_generator").html(data.data);
				$("#modal_generator").modal({'backdrop':'static'});
				
				// alert(data);
				// console.log(data);
			},error:function(error){
				console.log(error);
			}
		});
	}

</script>



</html>