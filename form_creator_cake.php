<!DOCTYPE html>
<html lang="en">
<head>
	<title>CODYGENERATOR</title>
	<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="asset/css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="asset/css/animate.css">
	<link rel="stylesheet" type="text/css" href="asset/font/css/all.css">
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  <ul class="navbar-nav">
	    <li class="nav-item active">
	      <a class="nav-link  text-info" href="#"><i class="fa fa-code"></i> <span style="color: orange;">CODY</span>GENERATOR</a>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link pt-2" href="form_generator.php">Form Generator (HTML)</a>
	    </li>
	     <li class="nav-item">
	      <a class="nav-link pt-2" href="form_creator_cake.php">Form Generator (CakePHP)</a>
	    </li>
	  <!--   <li class="nav-item">
	      <a class="nav-link" href="#">Link</a>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link disabled" href="#">Disabled</a>
	    </li> -->
	  </ul>
	</nav>
	<div class="container-fluid">
		<div class="row mt-4 m-1">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-12">
						<h1>Create Form</h1>
						
					</div>

					<div class="col-sm-12">
						<span>Fill up the fields and create your form (Bootstrap 4 <small><b>support</b></small>)</span>
						<button class="btn btn-danger  btn-sm float-right" onclick="remove_form();"> -</button>
						<button class="btn btn-success mr-2 btn-sm float-right" onclick="add_form();"> +</button>
					<hr>
					</div>

					<div class="col-sm-12">
						<form id="all_form">
							<table class="table table-bordered table-sm">
								<thead>
									<tr>
										<th>INPUT TYPE</th>
										<th>ID</th>
										<th>NAME</th>
										<th>PLACEHOLDER</th>
										<th width="10%">COL</th>
									</tr>
								</thead>
								
								<tbody id="form_inputs">

									<tr id="form_0">
										<td>
											<select class="form-control" id="input_type_0" oninput="type_input(this.value,'0');">
												<option value="text">Text</option>
												<option value="number">Number</option>
												<option value="date">Date</option>
												<option value="select">Select</option>
												<option value="password">Password</option>
											</select>
										</td>
										<td><input type="text" id="input_id_0" class="form-control" autocomplete="off"></td>
										<td><input type="text" id="input_name_0" class="form-control" autocomplete="off"></td>
										<td><input type="text" id="input_placeholder_0" class="form-control" autocomplete="off"></td>
										<td><input type="number" max="12" id="input_col_0" class="form-control" autocomplete="off"></td>
									</tr>
								</tbody>
							</table>
						</form>
						<button class="btn btn-info" onclick="get_result_and_code();">Result</button>
						<input type="hidden" id="row_input_count" value="0">
					</div>
					
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-12 form-group">
						<br><br>
						<h1>Code</h1>
						<p>Html <small>BY:JL BARCELONA</small></p>
						<textarea class="form-control bg-dark text-light" readonly="" style="height:300px;" id="code_generator"></textarea>
					</div>
				</div>
			</div>
	</div>
</body>
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/popper.min.js"></script>
<script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
<script type="text/javascript" src="asset/js/sweetalert.min.js"></script>

<script type="text/javascript">
	var input_count = 0;
	var url = 'function/function.php';

	function type_input(type,id){

		if (type == 'date' || type == 'select') {
			$("#input_placeholder_"+id).attr('disabled', true);
		}else{
			$("#input_placeholder_"+id).attr('disabled', false);
		}

	}

	function add_form(){
		var data = '';
		var input_count = (Number($('#row_input_count').val()) + 1);

		data += '<tr id="form_'+input_count+'">';
		data += '<td>';
		data += '<select class="form-control" id="input_type_'+input_count+'" oninput="type_input(this.value,'+input_count+');">';
		data += '<option value="text">Text</option>';
		data += '<option value="number">Number</option>';
		data += '<option value="date">Date</option>';
		data += '<option value="select">Select</option>';
		data += '<option value="password">Password</option>';
		data += '</select>';
		data += '</td>';
		data += '<td><input type="text" id="input_id_'+input_count+'" class="form-control" autocomplete="off"></td>';
		data += '<td><input type="text" id="input_name_'+input_count+'" class="form-control" autocomplete="off"></td>';
		data += '<td><input type="text" id="input_placeholder_'+input_count+'" class="form-control" autocomplete="off"></td>';
		data += '<td><input type="number" max="12" id="input_col_'+input_count+'" class="form-control" autocomplete="off"></td>';
		data += '</tr>';

		$("#form_inputs").append(data);
		$('#row_input_count').val(input_count);
	}

	function remove_form(){
		var input_count = $("#row_input_count").val();

	    if (input_count == 0) {
	      $("#livestock_count").val(0);
	    }else{
	      // $("#row_id_"+num).html('');
	      $("#form_"+input_count).remove("#form_"+input_count);
	      $("#row_input_count").val(input_count - 1);
	    }
	}

	function reset_form(){
	  document.getElementById('all_form').reset();
	}


	function get_result_and_code(){
		var number = Number($("#row_input_count").val());
		var input_data = [];
		for (var i = 0; i <= number; i++) {
		      input_data.push('['+$("#input_type_"+i).val() +'~'+ $("#input_id_"+i).val() +'~'+ $("#input_name_"+i).val()  +'~'+ $("#input_placeholder_"+i).val()+'~'+$("#input_col_"+i).val()+']');
		 }

		var mydata = 'action=create_form_cake' + '&input_details=' + input_data;
		// alert(mydata);
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			dataType:'json',
			success:function(data){
				// console.log(data);
				$("#html_results").html(data.data);
				$("#code_generator").text(data.data);
				
				// alert(data);
				// console.log(data);
			}
		});
	}
</script>



</html>