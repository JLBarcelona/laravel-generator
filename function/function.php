<?php 
require('helper.php');

$action = $_POST['action'];

switch ($action) {
	case 'create_form':

		$data = array();	
		$result = '';
		$input_details = $_POST['input_details'];
		$datas = explode(',', $input_details);
		$count_data = count($datas) -1;

		$result .= '<div class="form-row">'."\n";

		for ($i=0; $i < count($datas) ; $i++) { 
			$new_array = explode('~', $datas[$i]);
			
			if (str_replace('[', '', $new_array[0]) == 'select') {
				$result .="".'<div class="form-group col-sm-'.str_replace(']', '', $new_array[4]).'">'."\n";
				$result .= "\t\t".'<select id="'.$new_array[1].'" name="'.$new_array[2].'" class="form-control"></select>'."\n";
				$result .="\t".'</div>'."\n";
			}else{
				$result .= "\t".'<div class="form-group col-sm-'.str_replace(']', '', $new_array[4]).'">'."\n";
				$result .=  "\t\t".'<input type="'.str_replace('[', '', $new_array[0]).'" id="'.$new_array[1].'" name="'.$new_array[2].'" placeholder="'.$new_array[3].'" class="form-control">'."\n";
				$result .= "\t\t".'<div class="invalid-feedback" id="err_'.$new_array[1].'"></div>'."\n";
				$result .=  "\t".'</div>'."\n";
			}
			// echo $new_array[0].' '.$new_array[1].' '.$new_array[2].' '.$new_array[3].' '.$new_array[4];
		}

		$result .= "\n\t".'<div class="col-sm-12 text-right">'."";
		$result .= "\n\t\t".'<button class="btn btn-success" onclick="NAME_OF_FUNCTION()">Submit</button>'."";
		$result .= "\n\t".'</div>'."";

		$result .= "\n".'</div>'."";

		$result .= "\n\n\n".'<!-- Javascript Function-->'."";


		$result .= "\n".'<script>'."";
			// Javascript function
			$result .= "\n".'function NAME_OF_FUNCTION(){'."";
			// javascript function variable
				for ($ii=0; $ii < count($datas) ; $ii++) { 
						$arr = explode('~', $datas[$ii]);
						$result .= "\n\t".'var '.$arr[1]." = "."$(\"#".$arr[1]."\");";
				}

			// javascript function conditions
				for ($iii=0; $iii < count($datas) ; $iii++) { 
						$arrr = explode('~', $datas[$iii]);
						if ($iii == 0) {
							$result .= "\n\t".'if('.$arrr[1].'.val() == "" || '.$arrr[1].'.val() == null){'."";
								$result .= "\n\t\t".''.$arrr[1].'.focus();'."";
								$result .= "\n\t\t".'swal(\'Oops!\',\''.$arrr[3].' is required!\',\'error\');'."";
							$result .= "\n\t".'}'."";
							
						}else{
							$result .= "".'else if('.$arrr[1].'.val() == "" || '.$arrr[1].'.val() == null){'."";
								$result .= "\n\t\t".''.$arrr[1].'.focus();'."";
								$result .= "\n\t\t".'swal(\'Oops!\',\''.$arrr[3].' is required!\',\'error\');'."";
							$result .= "\n\t".'}'."";
						}
				}

			// javascript function data details
			$result .= "\n\telse{";
					$result .= "\n\t\tvar mydata = 'action=my_action '";

					for ($dt=0; $dt < count($datas) ; $dt++) { 
							$arr_dt = explode('~', $datas[$dt]);
							$result .= " + ".'\'&'.$arr_dt[1].'=\''." + ".$arr_dt[1].".val()";
					}

					$result .= ";";

					// javascript function ajax call
					$result .= "\n\t\t$.ajax({";
					$result .= "\n\t\t\ttype:\"POST\",";
					$result .= "\n\t\t\turl:url,";
					$result .= "\n\t\t\tdata:mydata,";
					$result .= "\n\t\t\tcache:false,";
					$result .= "\n\t\t\tbeforeSend:function(){";
					$result .= "\n\t\t\t\t\t//<!-- your before success function -->";
					$result .= "\n\t\t\t},";
					$result .= "\n\t\t\tsuccess:function(data){";
					$result .= "\n\t\t\t\tif(data.trim() == 1){";
						$result .= "\n\t\t\t\t\t//<!-- your success message or action here! -->";
					$result .= "\n\t\t\t\t}else{";
						$result .= "\n\t\t\t\t\t//<!-- your error message or action here! -->";
						$result .= "\n\t\t\t\t\tconsole.log(data.trim());";
					$result .= "\n\t\t\t\t}";

					$result .= "\n\t\t\t}";

					$result .= "\n\t\t});";



			$result .= "\n\t}";


			$result .= "\n".'}'."";

			$result .= "\n".'</script>'."";

			// php function

			$result .= "\n\n\n".'<!-- php Function here-->'."";
			$result .= "\n".'<!-- Put it in function/function.php -->'."";

			$result .= "\n".'<?php'."";
			$result .= "\n".'case \'my_action\':'."";
					for ($vars=0; $vars < count($datas) ; $vars++) { 
							$var_data = explode('~', $datas[$vars]);
							$result .= "\n\t".'$'.$var_data[1].' = '.'$_POST[\''.$var_data[1].'\'];';
					}


				$result .= "\n\t".'$data = array(';
				for ($ddt=0; $ddt < count($datas) ; $ddt++) { 
							$arr_dta = explode('~', $datas[$ddt]);

							if ($ddt == $count_data) {
								$result .= '\''.$arr_dta[1].'\''." => $".$arr_dta[1]."";
							}else{
								$result .= '\''.$arr_dta[1].'\''." => $".$arr_dta[1].", ";
							}
					}
				$result .= ');'."";
				
				$result .= "\n\t".'$response = '.'save($connection,\'Your table\',$data);';
				$result .= "\n\t".'if($response > 0){';
				$result .= "\n\t\t".'echo 1;';
				$result .= "\n\t".'}';


			$result .= "\n".'break;'."";
			$result .= "\n".'?>'."";



		$data = array('result' => true, 'data' => $result);

		echo json_encode($data);

	break;

	case 'laravel_form':

		$data = array();
		$result = '';
		$controller = $_POST['controller'];
		$form_id = $_POST['form_id'];
		$input_details = $_POST['input_details'];
		$basename = $_POST['basename'];
		$model = $_POST['model'];
		$base_id = $_POST['base_id'];


		$request_base_id = '$'.$base_id.'= $request->get(\''.$base_id.'\');';

		$request_base_var = '$'.$base_id;

		// $cont_src = explode('@', $src);

		$function_name_index = 'index';
		$function_name_list = 'list';
		$function_name_find = 'find';
		$function_name_add = 'add';
		$function_name_delete = 'trash';


		$url_list = $basename.'/list';
		$url_find = $basename.'/find/{'.$base_id.'}';
		$url_add = $basename.'/add';
		$url_delete = $basename.'/delete/{'.$base_id.'}';	
		$url_delete_parameter =  $basename.'/delete/';	
		$url_find_parameter =  $basename.'/find/';	


		$route_url_index = '';
		$route_url_list = 'list';
		$route_url_find = 'find/{'.$base_id.'}';
		$route_url_add = 'add';
		$route_url_delete = 'delete/{'.$base_id.'}';



		$src_index = $function_name_index;
		$src_list = $function_name_list;
		$src_find = $function_name_find;
		$src_add = $function_name_add;
		$src_delete = $function_name_delete;

		$datas = explode(',', $input_details);
		$count_data = count($datas) -1;

		$result .= '<div class="h3 p-2 bg-primary text-white mt-3 rounded">Html</div>';
		$result .= '<textarea class="form-control bg-dark text-light" readonly="" style="font-size:13px; height:450px;">';



		$result .= '<div class="row">'.line(1);
		$result .= tab(1).'<div class="col-lg-12 col-md-12 col-12">'.line(1);
		$result .= tab(2).'<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">'.line(1);
		$result .= tab(3).'<div class="mb-3 mb-md-0">'.line(1);
		$result .= tab(4).'<h1 class="mb-1 h2 fw-bold">'.str_replace('_', ' ', ucfirst($basename)).'</h1>'.line(1);
		$result .= tab(4).'@include(\'Layout.bread\', [\'data\' => [ \''.str_replace('_', ' ', ucfirst($basename)).'\' => url()->current() ] ])'.line(1);
		$result .= tab(3).'</div>'.line(1);
		$result .= tab(3).'<div class="d-flex">'.line(1);
		$result .= tab(4).'  <button class="btn btn-primary btn-shadow" onclick="add_'.$basename.'();"><i class="fa fa-plus"></i> Add '.str_replace('_', ' ', ucfirst($basename)).'</button>'.line(1);
		$result .= tab(3).'</div>'.line(1);
		$result .= tab(2).'</div>'.line(1);
		$result .= tab(1).'</div>'.line(1);
		$result .= '</div>'.line(2);



		$result .='<div class="card">'.line(1);
		$result .=tab(1).'<div class="card-body">'.line(1);
		$result .= tab(2).'<div class="table-responsive">'.line(1);
		$result .= tab(3).'<table class="table table-bordered dt-responsive nowrap" id="tbl_'.$basename.'" style="width: 100%;"></table>'.line(1);
		$result .= tab(2).'</div>'.line(1);
		$result .= tab(1).'</div>'.line(1);
		$result .='</div>'.line(2);

		

		$result .= '<form class="needs-validation" id="'.$form_id.'" action="{{ url(\''.$url_add.'\') }}" novalidate>'."\n\t";
		$result .='<div class="modal fade" role="dialog" id="modal_'.$basename.'" data-bs-backdrop="static">'.line(1);
		$result .= tab(1).'<div class="modal-dialog">'.line(1);
		$result .= tab(2).'<div class="modal-content">'.line(1);
		$result .= tab(3).'<div class="modal-header">'.line(1);
		$result .= tab(4).'<div class="modal-title" id="modal_title_'.$basename.'">'.line(1);
		$result .= tab(5).'Add '.$basename.line(1);
		$result .= tab(4).'</div>'.line(1);
		$result .= tab(4).'<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.line(1);
		$result .= tab(3).'</div>'.line(1);
		$result .= tab(3).'<div class="modal-body">'.line(1);
		$result .= tab(4).'<div class="row">'."\n\t";
		$result .=  tab(4).'<input type="hidden" id="'.$base_id.'" name="'.$base_id.'" placeholder="" class="form-control" required>'.line(1);
		for ($i=0; $i < count($datas) ; $i++) { 
			$new_array = explode('~', $datas[$i]);
			
			if (str_replace('[', '', $new_array[0]) == 'select') {
				$result .= tab(5).'<div class="position-relative mb-2 '.str_replace(']', '', $new_array[4]).'">'.line(1);
				$result .= tab(6).'<label>'.label($new_array[1]).'</label>'.line(1);
				$result .= tab(6).'<select id="'.$new_array[1].'" name="'.$new_array[1].'" class="form-control '.$new_array[2].'">'.line(1);
				
				$options = explode('-', $new_array[3]);
				foreach ($options as $option) {
					$result .= tab(7).'<option value="'.$option.'">'.$option.'</option>'.line(1);
				}
				$result .= tab(6).'</select>'.line(1);
				$result .= tab(6).'<div class="invalid-feedback" id="err_'.$new_array[1].'"></div>'.line(1);
				$result .= tab(5).'</div>'.line(1).tab(1);
			}else{
				$result .= tab(5).'<div class="position-relative mb-2 '.str_replace(']', '', $new_array[4]).'">'.line(1);
				$result .= tab(6).'<label>'.label($new_array[1]).'</label>'.line(1);
				$result .=  tab(6).'<input type="'.str_replace('[', '', $new_array[0]).'" id="'.$new_array[1].'" name="'.$new_array[1].'" placeholder="'.$new_array[3].'" class="form-control '.$new_array[2].'" required>'.line(1);
				$result .= tab(6).'<div class="invalid-feedback" id="err_'.$new_array[1].'"></div>'.line(1);
				$result .=  tab(5).'</div>'.line(1);
			}
			// echo $new_array[0].' '.$new_array[1].' '.$new_array[2].' '.$new_array[3].' '.$new_array[4];
		}
		$result .= tab(4).'</div>'.line(1);
		$result .= tab(3).'</div>'.line(1);
		$result .= tab(3).'<div class="modal-footer d-block">'.line(1);
		$result .= tab(4).'<div class="row">'.line(1);
		$result .= tab(5).'<div class="col-sm-6 col-12">'.line(1);
		$result .= tab(6).'<button class="btn btn-dark col-sm-12 col-12" data-bs-dismiss="modal" type="button">Cancel</button>'.line(1);
		$result .= tab(5).'</div>'.line(1);
		$result .= tab(5).'<div class="col-sm-6 col-12">'.line(1);
		$result .= tab(6).'<button class="btn btn-success col-sm-12 col-12" id="btn_submit_'.$basename.'" type="submit">Save</button>'.line(1);
		$result .= tab(5).'</div>'.line(1);
		$result .= tab(4).'</div>'.line(1);
		$result .= tab(3).'</div>'.line(1);
		$result .=tab(2).'</div>'.line(1);
		$result .= tab(1).'</div>'.line(1);
		$result .= tab(1).'</div>'.line(1);
		$result .='</form>';
		$result .= line(1).'</textarea>'."";

		

		$result .= '<div class="h3 p-2 bg-primary text-white mt-3 rounded">Script</div>';
		$result .= '<textarea class="form-control bg-dark text-light" readonly="" style="font-size:13px; height:600px;">';

		$result .= '<!-- Javascript Function-->'."";


		$result .= line(1).'<script>'."";

		$result .= line(1).tab(1).'show_'.$basename.'();';

		$result .= line(1).tab(1).'var tbl_'.$basename.';';
		$result .= line(1).tab(1).'function show_'.$basename.'(){';
		$result .= line(1).tab(2).'if (tbl_'.$basename.') {';
		$result .= line(1).tab(3).'tbl_'.$basename.'.destroy();';
		$result .= line(1).tab(2).'}';
		$result .= line(1).tab(2).'var url = main_path + \''.$url_list.'\';';
		$result .= line(1).tab(2).'tbl_'.$basename.' = $(\'#tbl_'.$basename.'\').DataTable({';
		$result .= line(1).tab(2).'pageLength: 10,';
		$result .= line(1).tab(2).'responsive: true,';
		$result .= line(1).tab(2).'ajax: url,';
		$result .= line(1).tab(2).'deferRender: true,';
		$result .= line(1).tab(2).'language: {';
		$result .= line(1).tab(2).'"emptyTable": "No data available"';
		$result .= line(1).tab(1).'},';
		$result .= line(1).tab(2).'columns: [';
		for ($iii=0; $iii < count($datas) ; $iii++) { 
		$new_array2 = explode('~', $datas[$iii]);
			$result .= '{';
			$result .= line(1).tab(2).'className: \'\',';
			$result .= line(1).tab(2).'"data": "'.$new_array2[1].'",';
			$result .= line(1).tab(2).'"title": "'.str_replace('_', ' ', ucfirst($new_array2[1])).'",';
			$result .= line(1).tab(1).'},';
		}
		$result .= '{';

		$result .= line(1).tab(2).'className: \'width-option-1 text-center\',';
		$result .= line(1).tab(2).'"data": "'.$base_id.'",';
		$result .= line(1).tab(2).'"orderable": false,';
		$result .= line(1).tab(2).'"title": "Options",';
		$result .= line(1).tab(3).'"render": function(data, type, row, meta){';
		$result .= line(1).tab(4).'newdata = \'\';';
		$result .= line(1).tab(4).'newdata += \'<button class="btn btn-success btn-sm font-base mt-1" data-id=\\\' \'+row.'.$base_id.'+\'\\\' onclick="edit_'.$basename.'(this)" type="button"><i class="fa fa-edit"></i></button> \';';
		$result .= line(1).tab(4).'newdata += \' <button class="btn btn-danger btn-sm font-base mt-1" data-id=\\\' \'+row.'.$base_id.'+\'\\\' onclick="delete_'.$basename.'(this)" type="button"><i class="fa fa-trash"></i></button>\';';
		$result .= line(1).tab(4).'return newdata;';
		$result .= line(1).tab(3).'}';
		$result .= line(1).tab(2).'}';
		$result .= line(1).tab(1).']';
		$result .= line(1).tab(1).'});';
		$result .= line(1).tab(1).'}';

		$result .= line(1);
		$result .= line(1).tab(1).'$("#'.$form_id.'").on(\'submit\', function(e){';
		// javascript function ajax call
		$result .= line(1).tab(2)."var url = $(this).attr('action');";
		$result .= line(1).tab(2)."var mydata = $(this).serialize();";
		$result .= line(1).tab(2).'e.stopPropagation();';
		$result .= line(1).tab(2).'e.preventDefault(e);'.line(1);

		$result .= line(1).tab(2)."$.ajax({";
		$result .= line(1).tab(3)."type:\"POST\",";
		$result .= line(1).tab(3)."url:url,";
		$result .= line(1).tab(3)."data:mydata,";
		$result .= line(1).tab(3)."cache:false,";
		$result .= line(1).tab(3)."beforeSend:function(){";
		$result .= line(1).tab(5)."//<!-- your before success function -->";
		$result .= line(1).tab(5).'$(\'#btn_submit_'.$basename.'\').prop(\'disabled\', true);';
		$result .= line(1).tab(5).'$(\'#btn_submit_'.$basename.'\').text(\'Please wait...\');';
		$result .= line(1).tab(3)."},";
		$result .= line(1).tab(3)."success:function(response){";
		$result .= line(1).tab(5)."//console.log(response)";
		$result .= line(1).tab(4)."if(response.status == true){";
		$result .= line(1).tab(5)."console.log(response)";
		$result .= line(1).tab(5).'swal("Success", response.message, "success");';
		$result .= line(1).tab(5)."showValidator(response.error,'".$form_id."');";
		$result .= line(1).tab(5).'show_'.$basename.'();';
		$result .= line(1).tab(5).'$(\'#modal_'.$basename.'\').modal(\'hide\');';
		$result .= line(1).tab(4)."}else{";
		$result .= line(1).tab(5)."//<!-- your error message or action here! -->";
		$result .= line(1).tab(5)."showValidator(response.error,'".$form_id."');";
		$result .= line(1).tab(4)."}";
		$result .= line(1).tab(4).'$(\'#btn_submit_'.$basename.'\').prop(\'disabled\', false);';
		$result .= line(1).tab(4).'$(\'#btn_submit_'.$basename.'\').text(\'Save\');';
		$result .= line(1).tab(3)."},";
		$result .= line(1).tab(3)."error:function(error){";
		$result .= line(1).tab(4)."console.log(error)";
		$result .= line(1).tab(3)."}";

		$result .= line(1).tab(2)."});";
		$result .= line(1).tab(1)."});";

	
		// delete script
		$result .= line(1);
		$result .= line(1).tab(1).'function delete_'.$basename.'(_this){';
		$result .= line(1).tab(2).'var id = $(_this).attr(\'data-id\');';
		$result .= line(1).tab(2).'var url =  main_path + \''.$url_delete_parameter.'\' + id;';
		$result .= line(1).tab(3).'swal({';
		$result .= line(1).tab(4).'title: "Are you sure?",';
		$result .= line(1).tab(4).'text: "Do you want to delete this '.$basename.'?",';
		$result .= line(1).tab(4).'type: "warning",';
		$result .= line(1).tab(4).'showCancelButton: true,';
		$result .= line(1).tab(4).'confirmButtonColor: "#DD6B55",';
		$result .= line(1).tab(4).'confirmButtonText: "Yes",';
		$result .= line(1).tab(4).'closeOnConfirm: false';
		$result .= line(1).tab(3).'},';
		$result .= line(1).tab(3).'function(){';
		$result .= line(1).tab(4).'$.ajax({';
		$result .= line(1).tab(4).'type:"delete",';
		$result .= line(1).tab(4).'url:url,';
		$result .= line(1).tab(4).'data:{},';
		$result .= line(1).tab(4).'dataType:\'json\',';
		$result .= line(1).tab(4).'beforeSend:function(){';
		$result .= line(1).tab(3).'},';
		$result .= line(1).tab(3).'success:function(response){';
		$result .= line(1).tab(4).'// console.log(response);';
		$result .= line(1).tab(4).'if (response.status == true) {';
		$result .= line(1).tab(5).'swal("Success", response.message, "success");';
		$result .= line(1).tab(5).'show_'.$basename.'();';
		$result .= line(1).tab(4).'}else{';
		$result .= line(1).tab(5).'console.log(response);';
		$result .= line(1).tab(4).'}';
		$result .= line(1).tab(3).'},';
		$result .= line(1).tab(3).'error: function(error){';
		$result .= line(1).tab(4).'console.log(error);';
		$result .= line(1).tab(3).'}';
		$result .= line(1).tab(3).'});';
		$result .= line(1).tab(2).'});';
		$result .= line(1).tab(1).'}';


		// Script add
		$result .= line(1);
		$result .= line(1).tab(1).'function add_'.$basename.'(){';
		$result .= line(1).tab(2).'$(\'#modal_title_'.$basename.'\').text(\'Add '.str_replace('_', ' ', ucfirst($basename)).'\');';
		$result .= line(1).tab(2).'$(\'#'.$base_id.'\').val(\'\');';
		$result .= line(1).tab(2)."showValidator([],'".$form_id."');";
		$result .= line(1).tab(2)."$('#".$form_id."').removeClass('was-validated');";
		for ($iii=0; $iii < count($datas) ; $iii++) { 
		$new_array2 = explode('~', $datas[$iii]);
		$result .= line(1).tab(2).'$(\'#'.$new_array2[1].'\').'.'val(\'\');';
		}
		$result .= line(1).tab(2).'$(\'#modal_'.$basename.'\').modal(\'show\');';

		$result .= line(1).tab(1).'}';

		// Script edit
		$result .= line(1);
		$result .= line(1).tab(1).'function edit_'.$basename.'(_this){';
		$result .= line(1).tab(2).'$(\'#modal_title_'.$basename.'\').text(\'Edit '.str_replace('_', ' ', ucfirst($basename)).'\');';
		$result .= line(1).tab(2).'var id = $(_this).attr(\'data-id\');';
		$result .= line(1).tab(2).'var url =  main_path + \''.$url_find_parameter.'\' + id;';
		$result .= line(1).tab(2).'$.ajax({';
		$result .= line(1).tab(2).'type:"get",';
		$result .= line(1).tab(2).'url:url,';
		$result .= line(1).tab(2).'data:{},';
		$result .= line(1).tab(2).'dataType:\'json\',';
		$result .= line(1).tab(3).'beforeSend:function(){';
		$result .= line(1).tab(2).'},';
		$result .= line(1).tab(2).'success:function(response){';
		$result .= line(1).tab(3).'let data = response.data;';
		$result .= line(1).tab(3).'$(\'#'.$base_id.'\').val(data.'.$base_id.');';
		for ($iii=0; $iii < count($datas) ; $iii++) { 
		$new_array2 = explode('~', $datas[$iii]);
		$result .= line(1).tab(3).'$(\'#'.$new_array2[1].'\').'.'val(data.'.$new_array2[1].');';
		}
		$result .= line(1).tab(3).'$(\'#modal_'.$basename.'\').modal(\'show\');';

		$result .= line(1).tab(2).'},';
		$result .= line(1).tab(2).'error: function(error){';
		$result .= line(1).tab(3).'console.log(error);';
		$result .= line(1).tab(2).'}';
		$result .= line(1).tab(2).'});';
		$result .= line(1).tab(1).'}';
		$result .= line(1).'</script>'."";
		$result .= '</textarea>';


		$result .= '<div class="h3 p-2 bg-primary text-white mt-3 rounded">Route</div>';
		$result .= '<textarea class="form-control bg-dark text-light" readonly="" style="font-size:13px; height:100px;">';

		// Route::controller(UserController::class)->group(function () {
		$result .= line(1).tab(0).'use App\Http\Controllers\\'.$controller.';';
		$result .= line(2).tab(0).'Route::controller('.$controller.'::class)->group(function(){';
		$result .= line(1).tab(1).'Route::group([\'prefix\'=> \''.$basename.'\', \'as\' => \''.$basename.'.\'], function(){';
		$result .= line(1).tab(2)." Route::get('".$route_url_index."', '".$src_index."')->name('index');";
		$result .= line(1).tab(2)." Route::get('".$route_url_list."', '".$src_list."')->name('list');";
		$result .= line(1).tab(2)." Route::get('".$route_url_find."', '".$src_find."')->name('find');";
		$result .= line(1).tab(2)." Route::post('".$route_url_add."', '".$src_add."')->name('add');";
		$result .= line(1).tab(2)." Route::delete('".$route_url_delete."', '".$src_delete."')->name('delete');";
		$result .= line(1).tab(1).'});';
		$result .= line(1).tab(0).'});';

		
		$result .= '</textarea>';

		$result .= '<div class="h3 p-2 bg-primary text-white mt-3 rounded">Laravel Function</div>';
		$result .= '<textarea class="form-control bg-dark text-light" readonly="" style="font-size:13px; height:600px;">';

			$result .= line(1).tab(0).'namespace App\Http\Controllers;';
			$result .= line(2).tab(0).'use Illuminate\Http\Request;';
			$result .= line(1).tab(0).'use App\Models\\'.$model.';';
			$result .= line(1).tab(0).'use Validator;';
			$result .= line(2).tab(0).'class '.$controller.' extends Controller';
			$result .= line(1).tab(0).'{';

			// $result .= line(2).tab(1).'<!-- Laravel function -->'."";
			$result .= line(1).tab(1).'function index(){';
			$result .= line(1).tab(2).'return view(\''.ucfirst($basename).'.index\');';
			$result .= line(1).tab(1).'}';

			$result .= line(2);

			$result .= line(1).tab(1).'function '.$function_name_list.'(){';
			$result .= line(1).tab(2).'$'.$basename.' = '.$model.'::whereNull(\'deleted_at\')->get();';
			$result .= line(1).tab(2).'return response()->json([\'status\' => true, \'data\' => $'.$basename.']);';
			$result .= line(1).tab(1).'}';

			$result .= line(2);


			$result .= line(1).tab(1).'function '.$function_name_find.'($'.$base_id.'){';
			$result .= line(1).tab(2).'$'.$basename.' = '.$model.'::where(\''.$base_id.'\', $'.$base_id.')->first();';
			$result .= line(1).tab(2).'return response()->json([\'status\' => true, \'data\' => $'.$basename.']);';
			$result .= line(1).tab(1).'}';

			$result .= line(2);

			$result .= tab(1).'function '.$function_name_add.'(Request $request){'.line(1);
			$result .= tab(2).$request_base_id.line(1);
			
			for ($iii=0; $iii < count($datas) ; $iii++) { 
				$new_array2 = explode('~', $datas[$iii]);
				$result .= tab(2).'$'.$new_array2[1].' = '.'$request->get(\''.$new_array2[1].'\');'.line(1);
			}
			// backend Validator
			$result .= line(1).tab(2).'$validator = Validator::make($request->all(), ['.line(1);


			for ($ii=0; $ii < count($datas) ; $ii++) { 
				$new_array1 = explode('~', $datas[$ii]);
				
				if (str_replace('[', '', $new_array1[0]) == 'email') {
					$result .= tab(3).'\''.$new_array1[1].'\''.' => '.'\''.'required|email'.'\','.line(1);
				}else{
					$result .= tab(3).'\''.$new_array1[1].'\''.' => '.'\''.'required'.'\','.line(1);
				}
			}

			$result .= tab(2).']);'.line(2);

			$result .= tab(2).'if ($validator->fails()) {'.line(1);
			$result .= tab(3).'return response()->json([\'status\' => false, \'error\' => $validator->errors()]);'.line(1);
			$result .= tab(2).'}else{'.line(1);

				$result .= tab(3).'if (!empty('.$request_base_var.')) {'.line(1);
							$result .= tab(4).'$'.$basename.' = '.$model.'::find('.$request_base_var.');'.line(1);
							for ($b=0; $b < count($datas) ; $b++) { 
								$new_array4 = explode('~', $datas[$b]);
								$result .= tab(4).'$'.$basename.'->'.$new_array4[1].' = '.'$'.$new_array4[1].';'.line(1);
							}

							$result .= tab(4).'if($'.$basename.'->save()){'.line(1);
							$result .= tab(5).'return response()->json([\'status\' => true, \'message\' => \''.ucfirst($basename).' updated successfully!\']);'.line(1);
							$result .= tab(4).'}'.line(1);

				$result .= tab(3).'}else{'.line(1);

							$result .= tab(4).'$'.$basename.' = new '.$model.';'.line(1);
							for ($a=0; $a < count($datas) ; $a++) { 
								$new_array3 = explode('~', $datas[$a]);
								$result .= tab(4).'$'.$basename.'->'.$new_array3[1].' = '.'$'.$new_array3[1].';'.line(1);
							}

							$result .= tab(4).'if($'.$basename.'->save()){'.line(1);
							$result .= tab(5).'return response()->json([\'status\' => true, \'message\' => \''.ucfirst($basename).' saved successfully!\']);'.line(1);
							$result .= tab(4).'}'.line(1);

				$result .= tab(3).'}'.line(1);

			$result .= tab(2).'}'.line(1);

		$result .= tab(1).'}'.line(1);


		$result .= line(1);
		$result .= line(1).tab(1).'function trash($'.$base_id.'){';
		$result .= line(1).tab(2).'$'.$basename.' = '.$model.'::find($'.$base_id.');';
		$result .= line(1).tab(2).'if($'.$basename.'->delete()){';
		$result .= line(1).tab(3).'return response()->json([\'status\' => true, \'message\' => \''.ucfirst($basename).' deleted successfully!\']);';

		$result .= line(1).tab(2).'}';

		$result .= line(1).tab(1).'}';
		$result .= line(1).tab(0).'}';

		$result .= '</textarea>';



		$data = array('result' => true, 'data' => $result);

		echo json_encode($data);

	break;


	case 'create_form_cake':
		$data = array();
		$result = '';
		$input_details = $_POST['input_details'];
		$datas = explode(',', $input_details);


		$result .= '<?= $this->Form->create() ?>'."\n";

		$result .= '<div class="row">'."\n";

		for ($i=0; $i < count($datas) ; $i++) { 
			$new_array = explode('~', $datas[$i]);

			if (str_replace('[', '', $new_array[0]) == 'select') {
				$result .="\t".'<div class="form-group col-sm-'.str_replace(']', '', $new_array[4]).'">'."\n";
				$result .=  "\t\t".'<?= $this->Form->control(\''.$new_array[1].'\', [\'class\' => \'form-control\', \'options\' => $option]) ?>'."\n";
				$result .="\t".'</div>'."\n";
			}else if (str_replace('[', '', $new_array[0]) == 'date') {
				$result .="\t".'<div class="form-group col-sm-'.str_replace(']', '', $new_array[4]).'">'."\n";
				$result .=  "\t\t".'<?= $this->Form->control(\''.$new_array[1].'\', [\'class\' => \'form-control\', \'data-provide\'=>\'datepicker\']) ?>'."\n";
				$result .="\t".'</div>'."\n";
			}else{
				$result .= "\t".'<div class="form-group col-sm-'.str_replace(']', '', $new_array[4]).'">'."\n";
				$result .=  "\t\t".'<?= $this->Form->control(\''.$new_array[1].'\', [\'type\' => \''.str_replace('[', '', $new_array[0]).'\', \'class\' => \'form-control\', \'placeholder\' => \''. $new_array[3].'\']) ?>'."\n";
				$result .=  "\t".'</div>'."\n";
			}
			// echo $new_array[0].' '.$new_array[1].' '.$new_array[2].' '.$new_array[3].' '.$new_array[4];
		}

		$result .= '</div>'.""."\n\n";

		$result .='<button type="submit" class="btn btn-primary mr-2">Submit</button>'."\n";
		$result .='<button type="submit" class="btn btn-danger">Cancel</button>'."\n";

		$result .= '<?= $this->Form->end() ?>';

		// echo $result;

		$data = array('result' => true, 'data' => $result);

		echo json_encode($data);
	break;
	
	default:
		echo 'Undefined '.$action; 
	break;
}





 ?>